package br.com.papodecafeteria.model;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class UserJPA{
	private static Logger l = Logger.getLogger(UserJPA.class.getName());
	
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true)
	private int 	iId;
	
	@Column(name = "name", nullable = false)
	private String	cName;
	
	@Column(name = "password", nullable = false)
	private String 	cPassword;
	
	@Column(name = "email", nullable = false)
	private String 	cEmail;
	
	@Column(name = "sex", nullable = false)
	private String 	cSex;
	
	@Column(name = "country", nullable = false)
	private String 	cCountry;
	
	public UserJPA(){
		super();
	}
	
	public UserJPA(int pId, String pName, String pPassword, String pEmail, String pSex, String pCountry){
		super();
		setcName(pName);
		setiId(pId);
		setcPassword(pPassword);
		setcEmail(pEmail);
		setcSex(pSex);
		setcCountry(pCountry);
	}

	public int getiId() {
		return iId;
	}

	public void setiId(int iId) {
		this.iId = iId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcPassword() {
		return cPassword;
	}

	public void setcPassword(String cPassword) {
		this.cPassword = cPassword;
	}

	public String getcEmail() {
		return cEmail;
	}

	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}

	public String getcSex() {
		return cSex;
	}

	public void setcSex(String cSex) {
		this.cSex = cSex;
	}

	public String getcCountry() {
		return cCountry;
	}

	public void setcCountry(String cCountry) {
		this.cCountry = cCountry;
	}
	
	@Override
	public String toString(){
		String cReturn = "";
		try {
			cReturn = "UserJPA [id = " + getiId() + ", email = " + getcEmail() + ", sex = " + getcSex() + ", country = " + getcCountry() + "]";
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
}
