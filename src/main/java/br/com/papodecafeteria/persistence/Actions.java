package br.com.papodecafeteria.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.model.UserJPA;

public class Actions {
	private static Logger l = Logger.getLogger(Actions.class.getName());
	
	public static List<UserJPA> getAll(){
		List<UserJPA> lstUserJPA = null;
		try {
			lstUserJPA = (List<UserJPA>) Core.exec("getAll", null);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return lstUserJPA;
	}
	
	public static UserJPA getById(UserJPA pUserJPA){
		UserJPA userJPA = null;
		try {
			userJPA = (UserJPA) Core.exec("getById", (Object) pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return userJPA;
	}
	
	public static void delete(UserJPA pUserJPA){
		try {
			Core.exec("delete", (Object) pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static UserJPA update(UserJPA pUserJPA){
		UserJPA userJPA = null;
		try {
			userJPA = (UserJPA) Core.exec("update", (Object) pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return userJPA;
	}
	
	public static UserJPA insert(UserJPA pUserJPA){
		UserJPA userJPA = null;
		try {
			userJPA = (UserJPA) Core.exec("insert", (Object) pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return userJPA;
	}
}
