package br.com.papodecafeteria.persistence;

import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import br.com.papodecafeteria.model.UserJPA;
import br.com.papodecafeteria.utilities.HibernateUtil;
import br.com.papodecafeteria.utilities.Util;

public class Core {
	private static Logger l = Logger.getLogger(Core.class.getName());

	public static Object exec(String pAction, Object pObj){
        Session session = null;
        Transaction transaction = null;
		Object obj = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			if(pAction.equalsIgnoreCase("getById")){
				obj = (Object) session.find( UserJPA.class, ((UserJPA) pObj).getiId());
			} else if(pAction.equalsIgnoreCase("getAll")){
			      List<UserJPA> lstUserJPA = (List<UserJPA>) session.createQuery("FROM UserJPA").list();
			      obj = (Object) lstUserJPA;
			} else {
				transaction = session.beginTransaction();
				if(pAction.equalsIgnoreCase("insert")){
					session.save((UserJPA) pObj);
					obj = pObj;
				} else if(pAction.equalsIgnoreCase("update")){
					UserJPA userJPA = (UserJPA) pObj;
					UserJPA userJPA2Update = session.find( UserJPA.class, userJPA.getiId() );
					obj = (Object) Util.mergeUserJPA2Update(userJPA2Update, userJPA);
					session.update(userJPA2Update);
				} else if(pAction.equalsIgnoreCase("delete")){
					session.delete((UserJPA) pObj);
				}
				transaction.commit();
			}
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
			if(null != transaction && transaction.isActive())
				transaction.rollback();
		} finally {
			if(null != session && session.isOpen())
				session.close();
		}
		return obj;
	}
}
