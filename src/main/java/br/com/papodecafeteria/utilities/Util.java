package br.com.papodecafeteria.utilities;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.model.UserJPA;

public class Util {
	private static Logger l = Logger.getLogger(Util.class.getName());
	
	public static UserJPA mergeUserJPA2Update(UserJPA pOld, UserJPA pNew){
		UserJPA userJPA = pOld;
		try { 
			userJPA.setcName(pNew.getcName());
			userJPA.setcEmail(pNew.getcEmail());
			userJPA.setcSex(pNew.getcSex());
			userJPA.setcCountry(pNew.getcCountry());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return userJPA;
	}
	
	public static void printUserJPA(UserJPA pUserJPA){
		try {
			System.out.println("(" + pUserJPA.getiId() + ") " + pUserJPA.getcName());
			System.out.println("\tEmail: " + pUserJPA.getcEmail());
			System.out.println("\tSex: " + pUserJPA.getcSex());
			System.out.println("\tCountry:" + pUserJPA.getcCountry() + "\n");
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void printListUserJPA(List<UserJPA> pListUserJPA){
		try {
			for(UserJPA userJPA : pListUserJPA)
				printUserJPA(userJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
}
