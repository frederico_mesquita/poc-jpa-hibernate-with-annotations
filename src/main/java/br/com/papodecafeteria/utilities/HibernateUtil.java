package br.com.papodecafeteria.utilities;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static Logger l = Logger.getLogger(HibernateUtil.class.getName());
	
    private static final SessionFactory sessionFactory = buildSessionFactory();
    
    private static SessionFactory buildSessionFactory() {
    	SessionFactory sessionFactory = null;
        try {
        	sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable exc) {
            l.log(Level.SEVERE, exc.getMessage(), exc);
        }
        return sessionFactory;
    }
  
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
  
    public static void shutdown() {
        getSessionFactory().close();
    }
}
