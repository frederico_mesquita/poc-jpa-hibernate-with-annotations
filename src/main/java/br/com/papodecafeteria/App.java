package br.com.papodecafeteria;

import br.com.papodecafeteria.model.UserJPA;
import br.com.papodecafeteria.persistence.Actions;
import br.com.papodecafeteria.utilities.Util;

public class App 
{
    public static void main( String[] args ) {
        System.out.println( "PoC JPA Hibernate with Annotations" );
        
        System.out.println("Listando todos os registros...\n");
        Util.printListUserJPA(Actions.getAll());
        
        System.out.println("Inserindo registro...\n");
        UserJPA userJPA = new UserJPA(0, "Fulanon de Tal", "pPassword", "email@email.com.br", "male", "Peru");
        userJPA = Actions.insert(userJPA);
        Util.printListUserJPA(Actions.getAll());
        Util.printUserJPA(userJPA);
        
        System.out.println("Atualizando registro inserido...\n");
        userJPA.setcName("Fulanon de Tal Updated");
        userJPA.setcEmail("email@email.updated.com.br");
        userJPA.setcCountry("Brazil");
        userJPA = Actions.update(userJPA);
        Util.printListUserJPA(Actions.getAll());
        Util.printUserJPA(userJPA);
        
        System.out.println("Excluindo registro inserido...\n");
        Actions.delete(userJPA);
        Util.printListUserJPA(Actions.getAll());        
    }
}
